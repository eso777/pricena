-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 06, 2020 at 07:49 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_pricena`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email_address`, `password`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$8B.IfkVUPXwCJ7JxVXUzNuUkUlI1rEvvJuWk02NDz7B7fdhVmmCxu', '0123456789', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Mobiles'),
(2, 'Cameras');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2020_05_21_000000_create_admins_table', 1),
(3, '2020_05_21_183847_create_categories_table', 1),
(4, '2020_05_21_184114_create_sub_categories_table', 1),
(5, '2020_05_21_230908_create_sellers_table', 1),
(6, '2020_05_24_183658_create_products_table', 1),
(7, '2020_05_27_222913_create_product_images_table', 1),
(8, '2020_05_28_005941_create_product_sellers_table', 1),
(9, '2020_06_05_084416_create_product_reviews_table', 1),
(10, '2020_06_05_095933_create_system_feedback_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `category_id`, `sub_category_id`, `created_at`, `updated_at`) VALUES
(1, 'New Camera', 'this is a best camera you gonna paid', 2, 5, '2020-06-05 09:27:26', '2020-06-05 09:27:26'),
(2, 'Iphone 11', 'This is no words to describe it', 1, 1, '2020-06-05 09:28:45', '2020-06-05 09:28:45'),
(3, 'Samsung note 10', 'Samsung note 10 Laboris exercitation.', 1, 2, '2020-06-05 09:29:30', '2020-06-05 09:29:30'),
(4, 'Cemera', 'Perferendis et ut qu', 2, 4, '2020-06-05 09:30:37', '2020-06-05 09:30:37'),
(5, 'Dexter Carr', 'In elit dolor ex et', 2, 4, '2020-06-05 13:06:21', '2020-06-05 13:06:21');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image_path`) VALUES
(1, 1, 'z84xecNDU27fvsi.jpg'),
(2, 1, 'diqXcpTiSslFTem.jpg'),
(3, 1, 'I1sXaTboOYEbtAq.jpeg'),
(4, 2, 'nzI8RriYIwmxpeE.jpeg'),
(5, 2, '0KOzWMXWBoTLx9X.png'),
(6, 2, 'CfBHlKkBZjeLWDE.jpg'),
(7, 2, '6vEqSKvQ8AKwcZz.webp'),
(8, 3, 'd5VJrrzsJaWmOYE.jpeg'),
(9, 4, 'VsADDRpTNIUe1y3.jpeg'),
(10, 4, 'fYu7ROYpzVvyGa1.jpg'),
(11, 4, 'pebRrQOiakOxImq.jpeg'),
(12, 5, 'WLbsDBhlsODDmlJ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `product_id`, `user_id`, `rating`, `review`, `created_at`, `updated_at`) VALUES
(1, 2, 14, 1, 'This bad product', '2020-06-05 13:43:33', '2020-06-05 13:43:33'),
(2, 2, 15, 5, 'Very Good', '2020-06-05 13:45:30', '2020-06-05 13:45:30'),
(3, 1, 14, 5, 'Tempore maiores est', '2020-06-05 14:17:21', '2020-06-05 14:17:21'),
(4, 5, 14, 4, 'Amet ullam ipsam cu', '2020-06-05 14:17:41', '2020-06-05 14:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_sellers`
--

CREATE TABLE `product_sellers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `seller_id` bigint(20) UNSIGNED NOT NULL,
  `sell_price` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_sellers`
--

INSERT INTO `product_sellers` (`id`, `product_id`, `seller_id`, `sell_price`) VALUES
(1, 1, 1, '8000.00'),
(2, 2, 1, '15000.00'),
(3, 3, 3, '9000.00'),
(4, 4, 5, '682.00'),
(5, 5, 4, '915.00');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `name`, `summary`, `address`, `email_address`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 'Tradeline', 'Eius dignissimos ad', 'In facere et laborum', 'zumine@mailinator.net', '+1 (377) 435-3316', '2020-06-05 09:23:17', '2020-06-05 09:23:17'),
(2, '2B', 'Dolores velit quia', 'Quisquam totam et re', 'jexixiremo@mailinator.com', '+1 (158) 709-5109', '2020-06-05 09:23:28', '2020-06-05 09:23:28'),
(3, 'Milan Story', 'Officiis odio doloru', 'Ratione laborum Obc', 'cywyd@mailinator.net', '+1 (894) 907-9682', '2020-06-05 09:23:39', '2020-06-05 09:23:39'),
(4, 'Milan Story', 'Officiis odio doloru', 'Ratione laborum Obc', 'cywyd@mailinator.net', '+1 (894) 907-9682', '2020-06-05 09:23:39', '2020-06-05 09:23:39'),
(5, 'El - Joker', 'Laboriosam voluptat', 'Expedita elit hic i', 'peqywefu@mailinator.com', '+1 (262) 819-8935', '2020-06-05 09:23:50', '2020-06-05 09:23:50'),
(7, 'Latifah Cash', 'Impedit Nam ut eum', 'Harum nisi cupidatat', 'gipe@mailinator.net', '+1 (919) 868-6613', '2020-06-05 12:58:09', '2020-06-05 12:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`) VALUES
(1, 'Apple', 1),
(2, 'Samsung', 1),
(3, 'Huawei', 1),
(4, 'Canon', 2),
(5, 'Nikon', 2),
(6, 'Panasonic', 2);

-- --------------------------------------------------------

--
-- Table structure for table `system_feedback`
--

CREATE TABLE `system_feedback` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `feedback` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_feedback`
--

INSERT INTO `system_feedback` (`id`, `user_id`, `feedback`, `created_at`, `updated_at`) VALUES
(2, 14, 'موقع زي العسل', '2020-06-05 13:58:55', '2020-06-05 13:58:55'),
(3, 14, 'Ut obcaecati nostrud', '2020-06-05 14:00:45', '2020-06-05 14:00:45'),
(4, 14, 'Facilis nostrud erro', '2020-06-05 14:01:32', '2020-06-05 14:01:32'),
(5, 14, 'Recusandae Nostrud', '2020-06-06 07:18:18', '2020-06-06 07:18:18'),
(6, 14, 'Quasi veniam molest', '2020-06-06 10:44:09', '2020-06-06 10:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `password`, `phone_number`, `email_address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Maggy', 'Hicks', '$2y$10$4bKnl5a0KQfbcPDSZKOi6.sRS/VV4EAWA4L6FzxnLbEPMRnAl2N1a', '+1 (371) 662-1908', 'lumigyzyb@mailinator.net', NULL, '2020-06-05 09:24:25', '2020-06-05 09:24:25'),
(2, 'Molly', 'Watson', '$2y$10$8NQK3uT1LAmLXmqfIuXEkuS/v1DmRgJ.wpdZI7NizV..JxuIuFYNm', '+1 (534) 241-4865', 'jovunypaq@mailinator.com', NULL, '2020-06-05 09:24:28', '2020-06-05 09:24:28'),
(3, 'Christine', 'Sharp', '$2y$10$7zSUJAp7QsYvwCQpwZ7uHOfDhzA95Tivy/T0pr4.YbXl973xslBwG', '+1 (232) 581-7423', 'xyhizalum@mailinator.com', NULL, '2020-06-05 09:24:30', '2020-06-05 09:24:30'),
(4, 'George', 'Leon', '$2y$10$rWOOy889HJwdedXDfJoSMOztOUcXESd5jFfpjZDn1DkG1SsRzrTLa', '+1 (485) 338-4249', 'bewazan@mailinator.com', NULL, '2020-06-05 09:24:54', '2020-06-05 09:24:54'),
(5, 'Paloma', 'Franklin', '$2y$10$lKQPp1DqH2ya4u6KJRY6K.s5xmDOcJ1PuW6eOgaieIMBuJt5UZrSu', '+1 (396) 654-2862', 'josyjur@mailinator.com', NULL, '2020-06-05 09:24:57', '2020-06-05 09:24:57'),
(6, 'Maite', 'Charles', '$2y$10$mBjT.S8Zhk8TPLk0ej823e8Gi/a0vB7Si7Dxzt0OaKmyFq7ZinFPe', '+1 (115) 261-9527', 'kykirugavy@mailinator.net', NULL, '2020-06-05 09:25:00', '2020-06-05 09:25:00'),
(7, 'Brandon', 'Guzman', '$2y$10$fYV0FMvCna2ozWt4aM6VVOH9JcWrJNFFB3AzqgJzQEl7/Ed19.V7a', '+1 (172) 254-8393', 'fujo@mailinator.net', NULL, '2020-06-05 09:25:03', '2020-06-05 09:25:03'),
(8, 'Anthony', 'Knox', '$2y$10$8wz8777EDe5iML0Ia.nve.Np6uKo8yNaL.qDkA61S.FmXxQwgkaVW', '+1 (812) 345-8515', 'sytetujuj@mailinator.net', NULL, '2020-06-05 09:25:08', '2020-06-05 09:25:08'),
(9, 'Chaney', 'Brooks', '$2y$10$unHUrqre/5ES3NcSeZ99H.M2Ha7agwzkjitYpIllIlVzXkG8WZ5FC', '+1 (166) 174-4233', 'zehiku@mailinator.net', NULL, '2020-06-05 09:25:12', '2020-06-05 09:25:12'),
(10, 'Magee', 'Rosa', '$2y$10$.EVP8UbdIFkvaSv3EXok7.4quoS8DWtWfN6utD3QB46/DChdGSIV6', '+1 (185) 295-5998', 'bylego@mailinator.net', NULL, '2020-06-05 09:25:15', '2020-06-05 09:25:15'),
(11, 'Hasad', 'Ochoa', '$2y$10$55Rc0Q20.yzFEZcNnnQwoOkZ4AT/7f/S4mpBvb2NViNkFYUxGszCe', '+1 (526) 815-2028', 'jikego@mailinator.com', NULL, '2020-06-05 09:25:18', '2020-06-05 09:25:18'),
(12, 'Hillary', 'Good', '$2y$10$VkU8Fu7OgxBPku.bUiinNO6tRx1hWuPlf7nU1itnAlZS6AbGb6gWK', '+1 (574) 716-9866', 'zocopeby@mailinator.net', NULL, '2020-06-05 09:25:28', '2020-06-05 09:25:28'),
(13, 'Harrison', 'Frazier', '$2y$10$tyEQZ0RG2sxW3ZCDvCi7DOmlPJ3asdX5LS85t5K5NLQsQeXmxihsu', '+1 (657) 764-4998', 'xufepi@mailinator.com', NULL, '2020-06-05 13:38:42', '2020-06-05 13:38:42'),
(14, 'Islam', 'Zekry', '$2y$10$x7y/YQLBKuIdo6DO037f7uBu3g8HuKPDuTU4Su16WpdCgPdLHQCKW', '5555555555555', 'user@gmail.com', 'AYPb5NwHZ66nNyo07voujSv4p1VTUmBXOoddkofiRPp8LO3OLORsjRqFt6vH', '2020-06-05 13:39:06', '2020-06-06 07:15:05'),
(15, 'Alec', 'Nelson', '$2y$10$EMPpl6ycbFMQx8mijtka1OSWbe81pryWpqOIG1k69FmB.xIZV7P/O', '+1 (158) 709-5109', 'xxx@gmail.com', NULL, '2020-06-05 13:44:57', '2020-06-05 13:44:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_address_unique` (`email_address`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_sellers`
--
ALTER TABLE `product_sellers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_sellers_product_id_foreign` (`product_id`),
  ADD KEY `product_sellers_seller_id_foreign` (`seller_id`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `system_feedback`
--
ALTER TABLE `system_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_feedback_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_address_unique` (`email_address`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_sellers`
--
ALTER TABLE `product_sellers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `system_feedback`
--
ALTER TABLE `system_feedback`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_sellers`
--
ALTER TABLE `product_sellers`
  ADD CONSTRAINT `product_sellers_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_sellers_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `system_feedback`
--
ALTER TABLE `system_feedback`
  ADD CONSTRAINT `system_feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
