<p>
    <i class="fa fa-sticky-note-o"></i>
    Product Reviews History
</p>

@if ($feedbackHistory->isNotEmpty())
<div class="container">
    <div class="row">

        @foreach($feedbackHistory as $feedback)

        <div class="card" style="width: 18rem;">
            <div class="card-body">

                <h5 class="card-title">{{ $feedback->user->first_name . ' ' . $feedback->user->last_name }}</h5>
                <p class="card-text">
                    {{ $feedback->feedback }}

                </p>
                <p>
                    Created Time : {{ \Carbon\Carbon::parse($feedback->created_at)->toDateString() }}
                </p>
            </div>
        </div>
        <br>

        @endforeach

    </div>
</div>
@else
<div class="alert alert-info">
    <i class="fa fa-sticky-note-o"></i>
    There's no any feedback, be the first person.
</div>
@endif
