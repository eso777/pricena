@extends('FrontEnd.Layouts.Layout')

@section('content')

    <div class="category-tab shop-details-tab">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#reviews" data-toggle="tab">
                        Give your feedback.
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="reviews">
                <div class="col-sm-12">
                    <p>
                        <b>
                            <i class="fa fa-sticky-note-o">
                                Your Feedback about out website.
                            </i>
                        </b>
                    </p>

                    {!! Form::open(['url'=> route('frontend.user.send-feedback'), 'class' => 'form-horizontal']) !!}

                        {!! Form::textarea('feedback', null, ['class' => 'form-control']) !!}

                        <button type="submit" class="btn btn-default pull-right">
                            <i class="fa fa-send"></i>
                            Submit
                        </button>

                    {!! Form::close() !!}

                </div>

                {{--  Feedback History --}}
                @include('FrontEnd.Feedback.feedback-history')

            </div>

        </div>
    </div>

@endsection
