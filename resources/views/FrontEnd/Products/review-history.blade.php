<p>
    <i class="fa fa-sticky-note-o"></i>
    Product Reviews History
</p>

@if ($product->reviews->isNotEmpty())
    <div class="container">
        <div class="row">

            @foreach($product->reviews as $review)

                <div class="card" style="width: 18rem;">
                    <div class="card-body">

                        <h5 class="card-title">{{ $review->user->first_name . ' ' . $review->user->last_name }}</h5>
                        <p class="card-text">
                            {{ $review->review }}

                        </p>
                        <p>
                            Rate : {{ $review->rating }}
                        </p>

                        <p>
                            Created Time : {{ \Carbon\Carbon::parse($review->created_at)->toDateString() }}
                        </p>

                    </div>
                </div>
                <br>

            @endforeach

        </div>
    </div>
@else
    <div class="alert alert-info">
        <i class="fa fa-sticky-note-o"></i>
        This product does not have any reviews before.
    </div>
@endif
