@extends('FrontEnd.Layouts.Layout')

@section('content')


    <h6 class="text-info"><i class="fa fa-sticky-note-o"></i> You maybe filter results by sub categories</h6>

    <br>

    <div class="filter-categories">

        {!! Form::open(['url'=> route('frontend.get-products-by-category', request('category_id')), 'class' => 'form-horizontal', 'method' => 'get']) !!}

        @foreach($subCategories as $subCategory)

            <label>
                {!! Form::checkbox('filters[]', $subCategory->id, in_array($subCategory->id, $filters) , ['class' => '']) !!}
                {{ $subCategory->name }}
            </label>

        @endforeach
        <br>
        <button type="submit" class="btn  pull-left">
            <i class="fa fa-filter"></i>
            Filter Result
        </button>

        {!! Form::close() !!}


    </div>

    @include('Partial.products', ['products' => $products])

</div>



@endsection
