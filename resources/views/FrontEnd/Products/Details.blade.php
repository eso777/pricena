@extends('FrontEnd.Layouts.Layout')

@section('content')

<div class="row">
    <h2>
        <i class="fa fa-info-circle"></i>
        {{ $product->name }}
    </h2>

    <div class="product-details"><!--product-details-->
        <div class="col-sm-5">
            <div class="view-product">
                <img src="{{ url('/Uploads/Products/'. $product->images()->first()->image_path ) }}" alt="" />
            </div>

            <h3>
                <i class="fa fa-info-circle"></i>
                Product Images
            </h3>
            <div id="similar-product" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    @foreach($product->images as $key => $image)
                        <div class="item {{{ $key == 0 ? 'active' : ''}}}">
                            <img src="{{ url('/Uploads/Products/'. $image->image_path ) }}" alt="">
                        </div>
                    @endforeach
                </div>

                <!-- Controls -->
                <a class="left item-control" href="#similar-product" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="right item-control" href="#similar-product" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>

        </div>
        <div class="col-sm-7">
            <div class="product-information"><!--/product-information-->
                <img src="{{ url('/FrontEnd') }}/images/product-details/new.jpg" class="newarrival" alt="" />
                <h2 class="text-info"> Name : {{ $product->name }}</h2>
                <h2> Description : {{ $product->description }}</h2>
                <img src="images/product-details/rating.png" alt="" />
                <span>
                    <span> {{ $product->productSeller[0]->pivot->sell_price }} EGP </span>
                </span>
                <p><b>Availability:</b> In Stock</p>
                <p><b>Condition:</b> New</p>
                <p><b>Brand:</b> E-SHOPPER</p>

                @if($productAvgReviews)
                    <p><b>Rate AVG:</b> {{ round($productAvgReviews, 1) }} Stars. </p>
                @endif

                <a href=""><img src="{{ url('/FrontEnd') }}/images/product-details/share.png" class="share img-responsive"  alt="" /></a>
            </div><!--/product-information-->
        </div>
    </div><!--/product-details-->

    @include('FrontEnd.Products.Review')
</div>

@endsection
