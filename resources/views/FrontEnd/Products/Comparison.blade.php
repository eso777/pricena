@extends('FrontEnd.Layouts.Layout')

@section('content')


{{--    @dump($products);--}}

<div class="row">

    @if(empty($products))
        <div class="alert alert-danger">
            There's no items in comparison now!
        </div>
    @else

        @if (count($products) < 2)

            {!! Form::open(['url'=> route('frontend.products.clear-comparison-list') , 'class' => 'form-horizontal']) !!}



            <div class="alert alert-danger">
               Your Comparison items is less than 2 products, So please select the next item to start compare. <button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                    Clear comparison List?
                </button>
            </div>

            {!! Form::close() !!}

        @else

            {!! Form::open(['url'=> route('frontend.products.clear-comparison-list') , 'class' => 'form-horizontal']) !!}

            <button class="btn btn-danger">
                <i class="fa fa-trash"></i>
                Clear comparison List?
            </button>

            {!! Form::close() !!}

            <br>
            <br>

            <div class="feature-table hide-550 pb0">
                <table class="table table-bordered table-hover table-responsive">
                    <tr>
                        <td class="feature-row-header">Name</td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->name }}
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-1">
                        <td class="feature-row-header">Description</td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->description }}
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-2">
                        <td class="feature-row-header">Category</td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->category->name ?? '-' }}
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-1">
                        <td class="feature-row-header">Sub Category</td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->subCategory->name ?? '-' }}
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-2">
                        <td class="feature-row-header">
                            Sell Price
                        </td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->productSeller[0]->pivot->sell_price }} EGP
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-1">
                        <td class="feature-row-header">
                            Seller
                        </td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->productSeller[0]['name'] ?? '-'}}
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-2">
                        <td class="feature-row-header">
                            Seller Address
                        </td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->productSeller[0]['address'] ?? '-'}}
                            </td>
                        @endforeach
                    </tr>
                    <tr class="feature-row-2">
                        <td class="feature-row-header">
                            Seller Phone Number
                        </td>
                        @foreach($products as $product)
                            <td class="feature-row-header">
                                {{ $product->productSeller[0]['phone_number'] ?? '-'}}
                            </td>
                        @endforeach
                    </tr>
                </table>
            </div>
        @endif

    @endif
</div>
@endsection
