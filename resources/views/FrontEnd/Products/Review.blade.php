<div class="category-tab shop-details-tab"><!--category-tab-->
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#reviews" data-toggle="tab">Reviews Or Rate Product</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="reviews">
            <div class="col-sm-12">
                @if (auth('user')->check())
                    <p>
                        <b>
                            <i class="fa fa-sticky-note-o">
                                Write Your Review Or Rate Product
                            </i>
                        </b>
                    </p>

                    {!! Form::open(['url'=> route('frontend.user.review-product', [$product->id]), 'class' => 'form-horizontal']) !!}

                    {!! Form::textarea('review', null, ['class' => 'form-control']) !!}

                    Rating:
                    <div class="product-review-stars">
                        <input type="radio" id="star5" name="rating" value="5" class="visuallyhidden" /><label for="star5" title="Pretty Good!">★</label>
                        <input type="radio" id="star4" name="rating" value="4" class="visuallyhidden" /><label for="star4" title="Good">★</label>
                        <input type="radio" id="star3" name="rating" value="3" class="visuallyhidden" /><label for="star3" title="Normal">★</label>
                        <input type="radio" id="star2" name="rating" value="2" class="visuallyhidden" /><label for="star2" title="Bad">★</label>
                        <input type="radio" id="star1" name="rating" value="1" class="visuallyhidden" /><label for="star1" title="Very Bad">★</label>
                    </div>

                    <button type="submit" class="btn btn-default pull-right">
                        <i class="fa fa-send"></i>
                        Submit
                    </button>

                    {!! Form::close() !!}

                @else
                    <div class="alert alert-info">
                        <i class="fa fa-sticky-note-o"></i>
                        Register or Login to your account to can give your Review Now.
                        <a href="{{ route('frontend.users.get-account-view') }}" class="btn btn-sm btn-warning">
                            <i class="fa fa-sign-in"></i>
                           Login to your Account?
                        </a>
                    </div>
                @endif
                    <br>
                    <hr>

                {{-- Product Reviews --}}
                @include('FrontEnd.Products.review-history')

            </div>
        </div>
    </div>
</div>
