@extends('FrontEnd.Layouts.Layout')

@section('content')

    <section id="form" style="margin-top: -15px"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="signup-form"><!--sign up form-->

                        <h2>
                            <i class="fa fa-edit"></i>
                            Update My Profile
                        </h2>

                        {!! Form::model($user, ['method' => 'PATCH', 'action' => ["FrontEnd\Users\UserController@updateUserProfile", $user->id], 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            First Name
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Last Name
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Phone Number
                            {!! Form::text('phone_number', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Email Address
                            {!! Form::email('email_address', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Password
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                            <small class="text-info">
                                <i class="fa fa-info-circle"></i>
                                Leave it blank so it won't change.
                            </small>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default ">
                                <i class="fa fa-sign-in"></i> Update Profile
                            </button>
                        </div>

                        {!! Form::close() !!}
                    </div><!--/sign up form-->
                </div>
            </div>
        </div>
    </section><!--/form-->

@endsection
