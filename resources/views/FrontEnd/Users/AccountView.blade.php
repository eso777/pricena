@extends('FrontEnd.Layouts.Layout')

@section('content')

    <h4>
        <i class="fa fa-sticky-note-o"></i>
        Register a new Account! Or Login
    </h4>

    <br>

    <section id="form" style="margin-top: -15px"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 ">
                    <div class="login-form"><!--login form-->
                        <h2>Login to your account</h2>
                        {!! Form::open(['action'=> "FrontEnd\Users\UserController@login", 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            Email Address
                            {!! Form::email('email_address', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Password
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                        <span>
                        <input type="checkbox" class="checkbox" name="remember_me">
                            Keep me signed in
                        </span>
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-sign-in"></i>
                            Login
                        </button>
                        </div>


                        {!! Form::close() !!}
                    </div><!--/login form-->
                </div>

                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>

                <div class="col-sm-4">
                    <div class="signup-form"><!--sign up form-->

                        <h2>New User Signup!</h2>

                        {!! Form::open(['action'=> "FrontEnd\Users\UserController@register", 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            First Name
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Last Name
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Phone Number
                            {!! Form::text('phone_number', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Email Address
                            {!! Form::email('email_address', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
                        </div>

                        <div class="form-group">
                            Password
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default ">
                                <i class="fa fa-sign-in"></i> Register
                            </button>
                        </div>

                        {!! Form::close() !!}
                    </div><!--/sign up form-->
                </div>
            </div>
        </div>
    </section><!--/form-->

@endsection
