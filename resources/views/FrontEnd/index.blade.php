@extends('FrontEnd.Layouts.Layout')

@section('content')

    @include('Partial.products', ['products' => $featuredProducts])

@endsection
