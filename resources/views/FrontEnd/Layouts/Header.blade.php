<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Welcome</title>
    <link href="{{ url('/FrontEnd') }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/FrontEnd') }}/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ url('/FrontEnd') }}/css/prettyPhoto.css" rel="stylesheet">
    <link href="{{ url('/FrontEnd') }}/css/price-range.css" rel="stylesheet">
    <link href="{{ url('/FrontEnd') }}/css/animate.css" rel="stylesheet">
    <link href="{{ url('/FrontEnd') }}/css/main.css" rel="stylesheet">
    <link href="{{ url('/FrontEnd') }}/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{ url('/FrontEnd') }}/js/html5shiv.js"></script>
    <script src="{{ url('/FrontEnd') }}/js/respond.min.js"></script>
    <![endif]-->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('/FrontEnd') }}/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ url('/FrontEnd') }}/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('/FrontEnd') }}/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="{{ url('/FrontEnd') }}/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
