<!DOCTYPE html>
<html>
@include('FrontEnd.Layouts.Header')

<body>
<header id="header"><!--header-->
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-md-4 clearfix">
                    <div class="logo pull-left">
                        <a href="{{ url('/') }}"><img src="{{ url('/FrontEnd') }}/images/home/logo.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-md-8 clearfix">
                    <div class="shop-menu clearfix pull-right">
                        <ul class="nav navbar-nav">
                            <li>

                                @if (auth('user')->check() == false)
                                    <a href="{{ route('frontend.users.get-account-view') }}">
                                        <i class="fa fa-user"></i> Account
                                    </a>
                                    <a href="{{ route('frontend.products.get-comparison-view') }}" class="btn btn-default">
                                        Compare
                                    </a>
                                @else
                                    <a href="#">
                                        <i class="fa fa-user"></i>Welcome, {{ auth('user')->user()->first_name . ' ' . auth('user')->user()->last_name }}
                                    </a>

                                    <a href="{{ route('frontend.users.get-update-profile-form') }}" class="btn btn-default btn-sm">
                                        <i class="fa fa-edit"></i> Update Profile
                                    </a>

                                    <p>
                                        {!! Form::open(['action'=> "FrontEnd\Users\UserController@logout", 'class' => 'form-horizontal']) !!}

                                        <button type="submit" class="btn btn-default btn-sm">
                                            <i class="fa fa-sign-in"></i>
                                            Logout
                                        </button>

                                        {!! Form::close() !!}
                                    </p>

                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

</header><!--/header-->

<section>

    <div class="container">
        <nav aria-label="">
            <ol class="breadcrumb" style="background-color: #000000">
                <li class="breadcrumb-item">
                    <a style="color: #ffffff" href="{{ route('frontend.home') }}">
                        <i class="fa fa-home"></i>
                        Home
                    </a>
                </li>
                <li class="breadcrumb-item pull-right">
                    <a style="color: #ffffff" href=" {{ route('frontend.user.send-feedback-view') }}">
                        <i class="fa fa-info-circle"></i>
                        Give System Feedback
                    </a>
                </li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-sm-3" style="margin-top: 60px !important;">

                @include('FrontEnd.Layouts.Sidebar-categories')
            </div>

            <div class="col-sm-9 padding-right">
                <br>
                <!-- Load Messenger Handler -->
                @include('Partial.messenger')
                <br>
                @yield('content')
            </div>
        </div>
    </div>
</section>

@include('FrontEnd.Layouts.Main-Footer')

</body>
</html>
