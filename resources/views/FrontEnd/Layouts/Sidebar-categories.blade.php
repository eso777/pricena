<style>
    .shipping {
        margin-bottom: 300px;
    }
</style>

<div class="left-sidebar">
    <h2>Categories</h2>
    <div class="panel-group category-products" id="accordian"><!--category-products-->

        {{--

            // Load Side-bar Categories with sub categories.
            // Please be notes that $categoriesWithProducts variable is defined in the AppServiceProvider Class in the boot mehtod
            // whenever application is Loaded the variable is defined as well.

        --}}

        @foreach($categoriesWithProducts as $category)

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a  href="{{ route('frontend.get-products-by-category', [$category->id]) }}">
                            {{$category->name}}
                        </a>
                    </h4>
                </div>
{{--                <div id="{{$category->name}}" class="panel-collapse collapse">--}}
{{--                    <div class="panel-body">--}}
{{--                        <ul>--}}
{{--                            @foreach($category->subCategories as $subCategory)--}}
{{--                                <li>--}}
{{--                                    <a href="#">{{ $subCategory->name }} </a>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

        @endforeach

    </div><!--/category-products-->

    <div class="shipping text-center"><!--shipping-->
        <img src="{{ url('/FrontEnd') }}/images/home/shipping.jpg" alt="" />
    </div><!--/shipping-->
</div>
