
    @if(Session()->has('success'))

        <div class="alert alert-success" role="alert">
            <i class="fa fa-sticky-note"></i>
            {!! Session()->get('success') !!}
        </div>

    @elseif(Session()->has('info'))

        <div class="alert alert-info" role="alert">
            <i class="fa fa-sticky-note"></i>
            {!! Session()->get('info') !!}
        </div>

    @elseif(Session()->has('warning'))

        <div class="alert alert-warning" role="alert">
            <i class="fa fa-sticky-note"></i>
            {!! Session()->get('warning') !!}
        </div>

    @elseif(Session()->has('danger'))
        <div class="alert alert-danger" role="alert">
            <i class="fa fa-sticky-note"></i>
            {!! Session()->get('danger') !!}
        </div>
    @elseif(Session()->has('product-added'))
        <div class="alert alert-info" role="alert">
            <i class="fa fa-sticky-note"></i>
            {!! Session()->get('product-added') !!}
            <a class="" href="{{ route('frontend.products.get-comparison-view') }}">
                Click Here
            </a>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="error-bag list-group">
                @foreach ($errors->all() as $error)
                    <li class="">
                        <i class="fa fa-times-circle-o"></i> {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

