<div class="features_items" style="margin-top: 60px !important;"><!--features_items-->
    <h2 class="title text-center">Features Items</h2>

    @if ($products->isNotEmpty())
        @foreach($products as $product)
            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            <img src="{{ url('/Uploads/Products/'. $product->images()->first()->image_path ) }}" alt="" width="150" height="200"/>
                            <h2>
                                {{ $product->productSeller[0]->pivot->sell_price . ' EG' ?? '_' }}
                            </h2>
                            <p>
                                {{ str_limit($product->description, 25) }}
                            </p>
                            <span class="badge badge-dark">{{ $product->category->name }}</span>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li>
                                        <a href="{{ route('frontend.products.show', [$product->id]) }}">
                                            <i class="fa fa-plus-square"></i>
                                            View Details
                                        </a>
                                    </li>
                                    <li>
                                        {!! Form::open(['url'=> route('frontend.products.add-to-compare', [$product->id]), 'class' => 'form-horizontal']) !!}

                                            <button type="submit">
                                                <i class="fa fa-plus-square"></i>
                                                Add to compare
                                            </button>

                                        {!! Form::close() !!}

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    @else
        <div class="alert alert-info">
            There's no data to show.
        </div>
    @endif

</div><!--features_items-->

{{ $products->links() }}
</div>
