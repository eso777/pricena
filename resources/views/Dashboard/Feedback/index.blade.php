@extends('Dashboard.Layouts.Layout')
@section('page-name', 'FeedBack')

@section('content')

    <div class="row">
        <div class="container-lg">
            <div class="panel-body">

                @if($feedback->isNotEmpty())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th># ID</th>
                            <th>Feedback</th>
                            <th>Created At</th>
                            <th>Options </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($feedback as $one)
                            <tr>
                                <td>{{ $one->id }}</td>
                                <td>{{ $one->feedback }}</td>
                                <td>{{ $one->created_at }}</td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'action' => ['Dashboard\Feedback\FeedbackController@destroy', $one->id]]) !!}

                                    {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-sm btn-circle', 'onclick' => "return confirm('هل متأكد من الحذف ؟');"] ) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $feedback->links() }}
                @else
                    <div class="alert alert-info">There's no data to show.</div>
                @endif
            </div>
        </div>
    </div>

    <!-- END PAGE LEVEL PLUGINS -->
@endsection
