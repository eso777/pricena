@extends('Dashboard.Layouts.Layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <div role="form">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-10">
                                    {!! Form::model($product, ['method' => 'PATCH', 'action' => ["Dashboard\Products\ProductController@update", $product->id], 'class' => 'form-horizontal','files' => true]) !!}
                                    @include('Dashboard.Products._form', ['type' => 'edit'])
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
