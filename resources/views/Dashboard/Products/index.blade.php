@extends('Dashboard.Layouts.Layout')
@section('page-name', 'Products')

@section('content')

    <div class="row">
        <div class="container-lg">
            <div class="buttons-actions">
                <a class="btn btn-primary btn-circle btn-sm" href="{{ route('products.create') }}">
                    <i class="fa fa-plus"></i>
                    Add New
                </a>

            </div>
            <br>
            <div class="panel-body">

                @if($products->isNotEmpty())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th># ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Created At</th>
                            <th>Options </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>{{ $product->SubCategory->name }}</td>
                                <td>{{ $product->created_at }}</td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'action' => ['Dashboard\Products\ProductController@destroy', $product->id]]) !!}

                                    <a href="{{ Url('/') }}/admin/products/{{ $product->id }}/edit" class="btn btn-info btn-sm"> <i class="fa fa-edit"></i>
                                    </a>

                                    {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-sm btn-circle', 'onclick' => "return confirm('هل متأكد من الحذف ؟');"] ) !!}

                                    <a class="btn btn-sm btn-primary" href="{{ route('products.show', [$product->id]) }}">
                                        <i class="fa fa-eye"></i>
                                        View Details
                                    </a>

                                    {!! Form::close() !!}


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                @else
                    <div class="alert alert-info">There's no data to show.</div>
                @endif
            </div>
        </div>
    </div>

    <!-- END PAGE LEVEL PLUGINS -->
@endsection
