@extends('Dashboard.Layouts.Layout')
@section('page-name', 'Product Details')
@section('content')
        <div class="row">
            <div class="col-md-12">
                <!-- /.card -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">
                            Product Name ( {{ $product->name }} )
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-text-height mr-1"></i>Product Name</strong>
                        <p class="text-muted">{{  $product->name }}</p>

                        <strong><i class="fas fa-pen mr-1"></i>Product Description</strong>
                        <p class="text-muted">{{  $product->description }}</p>

                        <strong><i class="fas fa-sim-card  mr-1"></i>Product Category</strong>
                        <p class="text-muted">{{  $product->category->name ?? '-' }}</p>

                        <strong><i class="fas fa-sim-card mr-1"></i>Product Sub Category</strong>
                        <p class="text-muted">{{  $product->SubCategory->name ?? '-' }}</p>

                        <strong><i class="fas fa-images mr-1"></i>Product Images</strong>
                        <p>
                            <div class="row">
                                @foreach($product->images as $image)

                                    <div class="col-md-3">
                                        <div class="thumbnail">

                                            <img src="{{ Url('Uploads/Products/' . $image->image_path) }}" width="150" height="150" class="image img-thumbnail">
                                            <div class="caption">
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </p>

                        <hr>

                        <strong><i class="fas fa-sim-card mr-1"></i>Seller Name</strong>
                        <p class="text-muted">{{  $product->productSeller[0]->name ?? '-' }}</p>

                        <strong><i class="fas fa-sim-card mr-1"></i>Sell Price</strong>
                        <p class="text-muted">{{  $product->productSeller[0]->pivot->sell_price ?? '-' }}</p>

                        <hr>

                        <strong>
                            <i class="fas fa-calendar-day mr-1"></i>
                            AVG Rate
                        </strong>
                        <p class="text-muted">
                            @if ($productAvgReviews)
                                {{  round($productAvgReviews)  }}
                            @else
                                Not Rated yet!
                            @endif
                        </p>

                        <strong>
                            <i class="fas fa-calendar-day mr-1"></i>
                            Created At
                        </strong>
                        <p class="text-muted">
                            {{  $product->created_at }}
                        </p>

                        <strong>
                            <i class="fas fa-calendar-day mr-1"></i>
                           Last Update Date
                        </strong>
                        <p class="text-muted">
                            {{  $product->updated_at }}
                        </p>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.card-body -->
@endsection
