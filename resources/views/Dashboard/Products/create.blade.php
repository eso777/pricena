@extends('Dashboard.Layouts.Layout')
@section('page-name', 'Product')

@section('content')

    <div class="row">
        <div class="col-md-12 col-md-auto">
            <div class="portlet light bordered">
                <div class="col-md-10">
                    {!! Form::open(['action'=> "Dashboard\Products\ProductController@store", 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('Dashboard.Products._form', ['action_title' => 'Add', 'type' =>'add'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
