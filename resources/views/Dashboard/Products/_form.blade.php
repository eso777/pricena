<style>
    img {
        margin-right: 10px;
        margin-bottom: 10px;
    }

    a.btn-danger {
        margin-bottom: 10px;
    }

</style>

<div class="form-group">
    <i class="fa fa-user-edit"></i>
    Product Name
    {!! Form::text('name', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
</div>

<div class="form-group">
    <i class="fa fa-user-edit"></i>
    Product Description
    {!! Form::textarea('description', null, ['class' => 'form-control', 'autocomplete' => 'nope', 'rows' => '4']) !!}
</div>

<div class="form-group">
    <label class="required"><i class="fa fa-sort"></i> Category </label>
    {!! Form::select('sub_category_id', $categories, null, ['class' => 'form-control', 'placeholder' => 'Sub Category that product belong with.']) !!}
</div>

<div class="form-group">
    <label class="required"><i class="fa fa-sort"></i> seller </label>
    {!! Form::select('seller_id', $sellers, $product->productSeller[0]->pivot->seller_id ?? null, ['class' => 'form-control', 'placeholder' => 'Choose The Seller']) !!}
</div>

<div class="form-group">
    <i class="fa fa-edit"></i>
    Sell Price
    {!! Form::text('sell_price', $product->productSeller[0]->pivot->sell_price ?? null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
</div>

@if($type == 'edit')

    <p>
        <i class="fa fa-images"></i>
        Current Images
    </p>

    <div class="row">

    @foreach($product->images as $image)

        <div class="col-md-3">
            <div class="thumbnail">

                <img src="{{ Url('Uploads/Products/' . $image->image_path) }}" width="150" height="150" class="image img-thumbnail">
                <div class="caption">
                    <a  class="btn btn-danger btn-sm btn-circle" href="{{ route('products.delete_image', [$image->id, $image->image_path]) }}" onclick = "return confirm('هل متأكد من الحذف ؟');">
                        <i class="fa fa-trash"></i>
                       Delete
                    </a>
                </div>
            </div>
        </div>

    @endforeach

    </div>
    <hr>
@endif

<div class="form-group">
    <label for="exampleFormControlFile1">
        <i class="fa fa-images"></i>
        Product Images
    </label>
    <input type="file"  name="images[]"  multiple class="form-control-file form-control">
</div>


{{-- Btns Controls--}}
<br>

<button type="submit" class="btn btn-primary">
    <i class="fa fa-save"></i>
    Save Data
</button>

<a href="{{route('sellers.index')}}" type="button" class="btn btn-warning">
    <i class="fa fa-times-circle"></i> Cancel
</a>

<br>
<br>
<br>
