@extends('Dashboard.Layouts.Layout')
@section('page-name', 'User Profile')
@section('content')
        <div class="row">
            <div class="col-md-12">
                <!-- /.card -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">
                            User Details ( {{ $user->first_name . ' ' . $user->last_name }} )
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong>
                            <i class="fas fa-user-edit mr-1"></i>
                            First Name
                        </strong>

                        <p class="text-muted">
                           {{  $user->first_name }}
                        </p>
                        <hr>

                        <strong>
                            <i class="fas fa-user-edit mr-1"></i>
                            Last Name
                        </strong>

                        <p class="text-muted">
                           {{  $user->last_name }}
                        </p>

                        <hr>

                        <strong>
                            <i class="fas fa-user-edit mr-1"></i>
                            Phone Number
                        </strong>

                        <p class="text-muted">
                           {{  $user->phone_number }}
                        </p>

                        <hr>

                        <strong>
                            <i class="fas fa-user-edit mr-1"></i>
                           Email Address
                        </strong>

                        <p class="text-muted">
                           {{  $user->email_address }}
                        </p>

                        <hr>

                        <strong>
                            <i class="fas fa-user-edit mr-1"></i>
                            Created At
                        </strong>

                        <p class="text-muted">
                            {{  $user->created_at }}
                        </p>

                        <hr>
                        <strong>
                            <i class="fas fa-user-edit mr-1"></i>
                           Last Update Date
                        </strong>

                        <p class="text-muted">
                            {{  $user->updated_at }}
                        </p>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.card-body -->
@endsection