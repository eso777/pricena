<!DOCTYPE html>
<html>
@include('Dashboard.Layouts.Header')
<body class="hold-transition login-page">
<h3 class="text-dark">
    Login to your Account.
</h3>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Pricena </b>V1.0</a>
    </div>

    {{--  To View messages Errors, Warning, info and so on  --}}
    @include('Partial.messenger')

    <!-- /.login-logo -->
    <div class="card">

        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            {!! Form::open(['url'=> Url('/admin/login'), 'class' => 'login-form', 'autocomplete' => 'off']) !!}
                <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Email" name="email_address">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="fa fa-sign-in"></i>
                            Sign In
                        </button>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">

                    </div>
                    <!-- /.col -->
                </div>
                {!! Form::close() !!}
            <!-- /.social-auth-links -->
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ Url('Dashboard') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ Url('Dashboard') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ Url('Dashboard') }}/js/adminlte.min.js"></script>

</body>
</html>

