<!DOCTYPE html>
<html>

@include('Dashboard.Layouts.Header')

<body class="hold-transition sidebar-mini layout-fixed">

<!-- ./wrapper -->
<div class="wrapper">

    <!-- Nav -->
@include('Dashboard.Layouts.Nav')
<!-- /.nav -->

    <!-- Main Sidebar Container -->
@include('Dashboard.Layouts.Sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            @yield('page-name')
                        </h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-lg-12 connectedSortable">

                        <!-- Load Messenger Handler -->
                            @include('Partial.messenger')

                        @yield('content')
                    </section>
                    <!-- /.Left col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->
    @include('Dashboard.Layouts.Main-Footer')
</div>
<!-- ./wrapper -->

@include('Dashboard.Layouts.Footer')

</body>
</html>
