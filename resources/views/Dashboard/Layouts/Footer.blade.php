<!-- jQuery -->
<script src="{{ Url('Dashboard') }}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ Url('Dashboard') }}/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ Url('Dashboard') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="{{ Url('Dashboard') }}/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="{{ Url('Dashboard') }}/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="{{ Url('Dashboard') }}/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="{{ Url('Dashboard') }}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ Url('Dashboard') }}/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{ Url('Dashboard') }}/plugins/moment/moment.min.js"></script>
<script src="{{ Url('Dashboard') }}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ Url('Dashboard') }}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{ Url('Dashboard') }}/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ Url('Dashboard') }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ Url('Dashboard') }}/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ Url('Dashboard') }}/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ Url('Dashboard') }}/js/demo.js"></script>