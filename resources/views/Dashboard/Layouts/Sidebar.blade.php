<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{Url('Dashboard')}}/img/AdminLTELogo.png" alt="Pricna Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">
            Pricena
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{Url('Dashboard')}}/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">
                    @auth()
                        {{ auth('web')->user()->name }}
                    @endauth
                </a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{ route('adminHomePage') }}" class="nav-link {{ \Request::is('admin') ? 'active' : '' }}">
                        <p>
                            <i class="fa fa-home"></i>
                            Home Page
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('products.index') }}" class="nav-link {{ \Request::is('products') ? 'active' : '' }}">
                        <p>
                            <i class="fa fa-list-ol"></i>
                            Products
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('users.index') }}" class="nav-link {{ \Request::is('users') ? 'active' : '' }}">
                        <p>
                            <i class="fa fa-users"></i>
                            Users
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('system-feedback.index') }}" class="nav-link {{ \Request::is('feedback') ? 'active' : '' }}">
                        <p>
                            <i class="fa fa-vote-yea"></i>
                            System Feedback
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('sellers.index') }}" class="nav-link {{ \Request::is('Sellers') ? 'active' : '' }}">
                        <p>
                            <i class="fa fa-users-cog"></i>
                            Sellers
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('categories.index') }}" class="nav-link {{ \Request::is('categories') ? 'active' : '' }}">
                        <p>
                            <i class="fa fa-users-cog"></i>
                            Categories
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link">
                        <p>
                            <i class="fa fa-sign-out-alt"></i>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
