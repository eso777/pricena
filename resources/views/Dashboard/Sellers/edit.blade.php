@extends('Dashboard.Layouts.Layout')
@section('title', $title ?? __('system.edit'))

@section('content')

    @stack('breadcrumb')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <div role="form">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-10">
                                    {!! Form::model($seller, ['method' => 'PATCH', 'action' => ["Dashboard\Sellers\SellerController@update", $seller->id], 'class' => 'form-horizontal','files' => true]) !!}
                                    @include('Dashboard.Sellers._form', ['text' => __('system.edit'), 'actionTitle' => __('system.edit') , 'type' => 'edit'])
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
