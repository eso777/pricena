@extends('Dashboard.Layouts.Layout')
@section('page-name', 'Sellers Information')
@section('content')

    <div class="row">
        <div class="container-lg">
            <div class="buttons-actions">
                <a class="btn btn-primary btn-circle btn-sm" href="{{ route('sellers.create') }}">
                    <i class="fa fa-plus"></i>
                    Add New
                </a>

            </div>
            <br>
            <div class="panel-body">

                @if($sellers->isNotEmpty())
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th># ID</th>
                            <th>Name </th>
                            <th>Summary</th>
                            <th>address</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th> Options </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sellers as $seller)
                            <tr>
                                <td>{{ $seller->id }}</td>
                                <td title="{{ $seller->name }}"> {{ $seller->name }} </td>
                                <td title="{{ $seller->summary }}"> {{ $seller->summary }} </td>
                                <td>{{ $seller->address }}</td>
                                <td>{{ $seller->email_address }}</td>
                                <td>{{ $seller->phone_number }}</td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'action' => ['Dashboard\Sellers\SellerController@destroy', $seller->id]]) !!}

                                    <a href="{{ Url('/') }}/admin/sellers/{{ $seller->id }}/edit" class="btn btn-info btn-sm"> <i class="fa fa-edit"></i>
                                    </a>
                                    {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-sm btn-circle', 'onclick' => "return confirm('هل متأكد من الحذف ؟');"] ) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $sellers->links() }}
                @else
                    <div class="alert alert-info">There's no data to show.</div>
                @endif
            </div>
        </div>
    </div>

    <!-- END PAGE LEVEL PLUGINS -->
@endsection
