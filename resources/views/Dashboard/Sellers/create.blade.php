@extends('Dashboard.Layouts.Layout')
@section('page-name', 'Seller Information')

@section('content')

    <div class="row">
        <div class="col-md-12 col-md-auto">
            <div class="portlet light bordered">
                <div class="col-md-10">
                    {!! Form::open(['action'=> "Dashboard\Sellers\SellerController@store", 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('Dashboard.Sellers._form', ['action_title' => 'Add', 'type' =>'add'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
