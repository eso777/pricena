<div class="form-group">
    Seller Name
    {!! Form::text('name', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
</div>

<div class="form-group">
    Phone Number
    {!! Form::text('phone_number', null, ['class' => 'form-control', 'autocomplete' => 'nope']) !!}
</div>

<div class="form-group">
    Email Address
    {!! Form::email('email_address', null, ['class' => 'form-control','placeholder' => 'eg: foo@bar.com','autocomplete'=>'nope']) !!}
</div>

<div class="form-group">
    Summary
    {!! Form::textarea('summary', null, ['class' => 'form-control', 'autocomplete' => 'nope', 'rows' => '2']) !!}
</div>

<div class="form-group">
    <label> Address</label>
    {!! Form::textarea('address', null, ['class' => 'form-control', 'autocomplete' => 'nope', 'rows' => '2']) !!}
</div>

<button type="submit" class="btn btn-primary">
    <i class="fa fa-save"></i>
    Save Data
</button>

<a href="{{route('sellers.index')}}" type="button" class="btn btn-warning">
    <i class="fa fa-times-circle"></i> Cancel
</a>
