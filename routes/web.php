<?php

/* Start Section Dashboard Routes  */

Route::group(['middleware' => ['web'], 'prefix' => 'admin', 'namespace' => 'Dashboard'], function () {

    Route::group(['middleware' => ['MustBeLoggedIn:web']], function () {

        # Dashboard Entry Point.
        Route::get('/', 'BackendIndexController@index')->name('adminHomePage');
        # Dashboard Entry Point.

        # Users Routes.
        Route::get('/users', 'Users\UserController@index')->name('users.index');
        Route::get('/users/{user_id}', 'Users\UserController@show')->name('users.show');
        # Users Routes.

        # Categories Routes.
        Route::get('/categories', 'Categories\CategoryController@index')->name('categories.index');
        Route::get('/categories/{category_id}', 'Categories\CategoryController@show')->name('categories.show_sub_categories');
        # Categories Routes.

        # Sellers Routes.
        Route::resource('sellers', 'Sellers\SellerController');
        # Sellers Routes.

        # Products Routes.
        Route::get('product/{image_id}/{image_name}/delete', 'Products\ProductController@deleteProductImage')->name('products.delete_image');
        Route::resource('products', 'Products\ProductController');
        # Products Routes.


        Route::resource('system-feedback', 'Feedback\FeedbackController')->only([
            'index', 'destroy'
        ]);

    });


    /* START ROUTING SECTION FOR AUTHENTICATION */

    Route::group(['namespace' => 'SystemAuthentication'], function () {
        Route::get('logout', 'AdminLogoutController@logout')->name('logout');
        Route::post('login', 'AdminLoginController@LoginToDashboard')->name('LoginToDashboard');
        Route::get('login', 'AdminLoginController@showAdminView')->name('loginView');
    });

    /* END ROUTING SECTION FOR AUTHENTICATION */
});

/* End Section Dashboard Routes  */


/* Start section Front-end Routes */

Route::group(['middleware' => ['web'], 'prefix' => '/', 'namespace' => 'FrontEnd'], function () {

    # Intro Page ( Home Page ).
    Route::get('/', 'FrontEndController@index')->name('frontend.home');

    # Start section routes user must be logged in to can access them.
    Route::group(['middleware' => ['MustBeLoggedIn:user']], function () {

        # User
        Route::post('user/review-product/{product_id}', 'Users\ReviewProductController@reviewProduct')->name('frontend.user.review-product');
        Route::get('user/system-feedback', 'Users\SystemFeedbackController@getSendFeedbackViewPage')->name('frontend.user.send-feedback-view');
        Route::post('user/system-feedback', 'Users\SystemFeedbackController@giveSystemFeedback')->name('frontend.user.send-feedback');
        # User

    });
    # End section routes user must be logged in to can access them.

    # Users
    Route::get('user', 'Users\UserController@getAccountView')->name('frontend.users.get-account-view'); // this page has view  for login and register.
    Route::post('user/register', 'Users\UserController@register')->name('frontend.users.register');
    Route::post('user/login', 'Users\UserController@login')->name('frontend.users.login');
    Route::post('user/logout', 'Users\UserController@logout')->name('frontend.users.logout');
    Route::get('user/update-profile', 'Users\UserController@GetUpdateProfileForm')->name('frontend.users.get-update-profile-form');
    Route::patch('user/update-profile', 'Users\UserController@updateUserProfile')->name('frontend.users.updateUserProfile');
    # Users

    # Products
    Route::get('products/comparison', 'Products\ProductComparison@getQueuedComparisonProducts')->name('frontend.products.get-comparison-view');
    Route::get('products/{product_id}', 'Products\ProductController@getProductDetails')->name('frontend.products.show');
    Route::post('products/{product_id}/add-to-compare', 'Products\ProductComparison@addProductToCompare')->name('frontend.products.add-to-compare');
    Route::post('products/clear-comparison-list', 'Products\ProductComparison@clearComparisonList')->name('frontend.products.clear-comparison-list');
    # Products

    # Categories
    Route::get('category/{category_id}/products', 'Products\ProductController@getProductByCategory')->name('frontend.get-products-by-category');
    # Categories
});

/* End section Front-end Routes */





