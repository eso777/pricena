<?php


namespace App\Traits;

use Illuminate\Support\Str;

trait FileUploader
{
    /**
     * @param $file
     *
     * @param $directory | The Directory that you need to upload file to it.
     *
     * @return string
     */
    public function uploadFile($file, $directory)
    {
        $destinationPath = public_path('/Uploads/') . $directory;

        // Check if the directory already exists.
        if(! is_dir($destinationPath)) {
            // Directory does not exist, so lets create it and make it writeable.
            mkdir($destinationPath, 0755);
        }

        // Give a Random name for the File.
        $file_name = Str::random('15') . '.' . $file->getClientOriginalExtension();

        // Move ( Upload it ).
        $file->move($destinationPath, $file_name);

        // and return the file name.
        return $file_name;
    }
}