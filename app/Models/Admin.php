<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Admin extends Authenticatable
{
    protected $table    = 'admins';

    protected $fillable = [ 'name', 'email_address', 'password', 'phone_number'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (Hash::needsRehash($value) && $value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }
}
