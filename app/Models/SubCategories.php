<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    protected $table = 'sub_categories';

    protected $fillable = [
        'id',
        'name',
        'category_id'
    ];

    /**
     * Get Parent Category for current sub category object.
     */
    public function subCategories()
    {
        return $this->belongsTo(Category::class);
    }
}

