<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name',
        'description',
        'category_id',
        'sub_category_id',
    ];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function subCategory()
    {
        return $this->belongsTo(SubCategories::class, 'sub_category_id', 'id');
    }

    /**
     * The Product Images.
     *
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }


    /**
     * @return BelongsToMany
     */
    public function productSeller()
    {
        return $this->belongsToMany(Seller::class, 'product_sellers', 'product_id', 'seller_id')
                    ->withPivot('seller_id', 'sell_price');
    }


    /**
     * @return HasMany
     */
    public function reviews()
    {
       return $this->hasMany(ProductReview::class)->latest();
    }
}
