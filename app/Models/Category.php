<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'id',
        'name'
    ];

    /**
     * Get Related Sub Category that belong to current category object.
     *
     * @return HasMany
     */
    public function subCategories()
    {
        return $this->hasMany(SubCategories::class, 'category_id', 'id');
    }
}
