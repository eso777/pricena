<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';

    protected $fillable = [
        'product_id',
        'image_path'
    ];

    public $timestamps = false; // we do not need created at & updated at in this Table so we will disable it.
}
