<?php

namespace App\Models;

use App\Traits\TimeStampTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{
    protected $table = 'users';

    protected $fillable = ['first_name', 'last_name', 'email_address', 'password', 'phone_number'];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (Hash::needsRehash($value) && $value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }


    /**
     * @return HasMany
     */
    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }


    /**
     * System Feedback relation
     *
     * @return HasMany
     */
    public function feedback()
    {
        return $this->hasMany(systemFeedback::class)->latest();
    }
}
