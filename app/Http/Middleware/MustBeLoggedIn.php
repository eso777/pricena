<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MustBeLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // CHECK IF ADMIN IS LOGGED IN
        if (! Auth::guard('web')->check() && $guard == 'web') {

            return redirect(route('loginView'));

        } else if (! Auth::guard('user')->check() && $guard == 'user') { // Check if user logged in otherwise redirect him to login view page in the front end

            // Front end Login view Page .
            return redirect(route('frontend.users.get-account-view'));
        }

        // is everything is fine go to the next request that user or admin need.
        return $next($request);
    }
}
