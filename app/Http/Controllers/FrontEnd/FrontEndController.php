<?php


namespace App\Http\Controllers\FrontEnd;


use App\Models\Category;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class FrontEndController
{
    /**
     * Get Front-end Index page.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        // Random Products to view them in intro page.
        $featuredProducts = Product::with('productSeller', 'images')->paginate(15);

        return view('FrontEnd.index', compact( 'featuredProducts'));
    }
}
