<?php


namespace App\Http\Controllers\FrontEnd\Users;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class UserController
{
    /**
     * Get Account View ( Login or Register Page ).
     */
    public function getAccountView()
    {
        // if User already logged in redirect to home page
        if (auth('user')->check() == true) {
            return redirect(route('frontend.home'));
        }

        return view('FrontEnd.Users.AccountView');
    }


    /**
     * Register a new user.`
     *
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function register(Request $request)
    {
        // Validate Request Data.
        $request->validate([
            'first_name'      => 'required|string|max:255',
            'last_name'       => 'required|string|max:255',
            'password'        => 'required|min:5',
            'phone_number'    => 'required|string',
            'email_address'   => 'required|email',
        ]);

        // Save User Data in a database.
        User::create($request->all());

        return redirect(route('frontend.users.get-account-view'))->with([
            'success' => 'Registered Successfully.'
        ]);
    }


    /**
     * Login in the system.
     *
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function login(Request $request)
    {
        if (auth('user')->check() == true) {
            return redirect(route('frontend.home'));
        }

        $request->validate([
            'email_address'    => 'required|email',
            'password'         => 'required',
        ]);

        $remember_me = $request->input('remember_me', false);

        if (auth('user')->attempt(['email_address' => $request->email_address , 'password' => $request->password], $remember_me)) {
            return redirect(route('frontend.home'));
        }

        return back()->withInput($request->all())->with([
            'danger' => 'Incorrect Email or password.'
        ]);
    }



    /**
     * Logout User
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function logout()
    {
        if (auth('user')->check()) {
            auth('user')->logout();
        }

        return redirect(route('frontend.users.get-account-view')) ;
    }



    /**
     * Get Update Profile Form view.
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function GetUpdateProfileForm()
    {
        $user = auth('user')->user();
        return view('FrontEnd.Users.update-profile', compact('user')) ;
    }


    /**
     * Update User Profile
     *
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function updateUserProfile(Request $request)
    {
        // Validate Request Data.
        $request->validate([
            'first_name'      => 'required|string|max:255',
            'last_name'       => 'required|string|max:255',
            'password'        => 'nullable|min:5',
            'phone_number'    => 'required|string',
            'email_address'   => 'required|email',
        ]);

        // Get Authenticated user. ( Logged in user ).
        $user = auth('user')->user();

        // Update and save data in database.
        $user->update($request->all());

        // Return successful message.
        return back()->with([
            'success' => 'Your Profile has been updated successfully!'
        ]);
    }


}
