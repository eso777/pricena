<?php


namespace App\Http\Controllers\FrontEnd\Users;


use App\Models\systemFeedback;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SystemFeedbackController
{
    /**
     * Send System FeedBack.
     *
     * @param Request $request
     *
     * @return Application|Factory|View
     */
    public function getSendFeedbackViewPage(Request $request)
    {
        $feedbackHistory = systemFeedback::latest()->get();

        return view('FrontEnd.Feedback.feedback', compact('feedbackHistory'));
    }


    /**
     * Send System FeedBack.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function giveSystemFeedback(Request $request)
    {
        // Validate Request Data.
        $request->validate([
            'feedback'  => 'required|string',
        ]);

        auth('user')->user()->feedback()->create($request->all());

        // Return back with success message.
        return back()->with([
            'success' => 'Your Review saved successfully, Thank you!'
        ]);
    }
}
