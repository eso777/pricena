<?php


namespace App\Http\Controllers\FrontEnd\Users;


use App\Models\ProductReview;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ReviewProductController
{
    /**
     * This method to make user give his review and rating for the product.
     *
     * @param Request $request
     *
     * @param $product_id
     *
     * @return RedirectResponse
     */
    public function reviewProduct(Request $request, $product_id)
    {
        // Validate Request Data.
        $request->validate([
            'review'     => 'required|string',
            'rating'     => 'required|min:1|max:5'
        ]);

        // Merge Product to request.
        $request->merge([
            'product_id' => $product_id
        ]);

        // Check if user Can Review ( not give review before for this product ).
        $isReviewedBefore = $this->CheckReviewAvailability($product_id);

        // if get true that mean user can't.
        if ($isReviewedBefore) {
            return back()->with([
                'info' => 'We\'re very sorry, you already reviewed this product before.'
            ]);
        }

        // Otherwise Adding a new review for this product and for the logged in user.
        auth('user')->user()->reviews()->create($request->all());

        // Return back with success message.
        return back()->with([
            'success' => 'Your Review saved successfully, Thank you!'
        ]);
    }



    /**
     * The user can review The product only one time
     * So we'll check if that user already give his review for that product already before or not
     *
     * @param $product_id
     *
     * @return mixed
     */
    private function CheckReviewAvailability($product_id)
    {
        return ProductReview::where([
            'user_id' => auth('user')->id(),
            'product_id' => $product_id
        ])->exists();
    }
}
