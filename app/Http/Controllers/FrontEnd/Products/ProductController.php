<?php


namespace App\Http\Controllers\FrontEnd\Products;

use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategories;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class ProductController
{
    /**
     * Get The Product Details for given product Id.
     *
     * @param $product_id
     *
     * @return Application|Factory|View
     */
    public function getProductDetails($product_id)
    {
        // Query to get Product by ID.
        $product = Product::findOrFail($product_id);

        $productAvgReviews = $product->reviews()->avg('rating');

        // return Product To view details Page.
        return view('FrontEnd.Products.Details', compact('product', 'productAvgReviews'));
    }


    /**
     * Get List of Products by Category
     *
     * @param Request $request
     *
     * @param $category_id
     *
     * @return Application|Factory|View
     */
    public function getProductByCategory(Request $request, $category_id)
    {
        // check if we have a filter
        $filters = $request->filters ?? [];

        // to re-build URL with filters
//        $filterUrl = "";
//
//        if (! empty($filters)) {
//
//            foreach ($filters as $one) {
//
//              if (empty($filterUrl)) {
//                  $filterUrl = '&filter=' . $one;
//              } else {
//                  $filterUrl .= '-' . $one;
//              }
//            }
//        }

        // Build Query to get products

        $query = Product::where('category_id', $category_id);

        if ($filters) {
            $query->whereIn('sub_category_id', $filters);
        }

        $products = $query->paginate();

        $subCategories = SubCategories::where('category_id', $category_id)->get();

        return view('FrontEnd.Products.products-by-category', compact('products', 'subCategories', 'filters'));
    }
}
