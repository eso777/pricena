<?php


namespace App\Http\Controllers\FrontEnd\Products;

use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;

class ProductComparison
{
    /**
     * This method to adding Product to Comparison Queue.
     *
     * @param $product_id
     *
     * @return RedirectResponse
     */
    public function addProductToCompare($product_id)
    {

        $products = Session::get('products', collect([]));

        if ($products->isEmpty()) {

            $products [] = Product::findOrFail($product_id);

            Session::put('products', $products);
            Session::save();

            return back()->with([
                'info' => 'Product add to Compare Successfully.'
            ]);

        } else {

            $product = Product::findOrFail($product_id);

            $currentQueuedItemsCategory = $products->pluck('category_id')->all();

            // max 3 items
            if (count($currentQueuedItemsCategory) < 3 == false) {
                return back()->with([
                    'danger' => 'Sorry maximum items you can compare is 3 items only'
                ]);
            }

            // Check if item is same kind ( same category )
            if (in_array($product->category_id, $currentQueuedItemsCategory) == false) {
                return back()->with([
                    'danger' => 'Sorry you must choose same category to can compare'
                ]);
            }

           if ($products->where('id', $product_id)->first() == null) {


               $products [] = $product;
               Session::put('products', $products);
               Session::save();

               return back()->with([
                   'product-added' => 'Product add to Compare if you need to goto Comparison Page'
               ]);
           }
        }

        return back()->with([
            'info' => 'This Product already added before.'
        ]);
    }


    /**
     *
     */
    public function getQueuedComparisonProducts()
    {
        $products = Session::get('products', []);

        return view('FrontEnd.Products.Comparison', compact('products'));
    }



    /**
     * @return RedirectResponse
     */
    public function clearComparisonList()
    {
        Session::forget('products');

        return redirect(route('frontend.products.get-comparison-view'))->with([
            'info' => 'Your Comparison List cleared successfully.'
        ]);
    }
}
