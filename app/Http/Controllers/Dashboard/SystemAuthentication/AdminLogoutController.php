<?php

namespace App\Http\Controllers\Dashboard\SystemAuthentication;

use App\Http\Controllers\Controller;

class AdminLogoutController extends Controller
{
    /* LOGOUT ADMIN */
    public function logout()
    {
        if (auth('web')->check()) {
            auth('web')->logout();
        }

        return redirect(route('loginView')) ;
    }
}
