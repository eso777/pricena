<?php

namespace App\Http\Controllers\Dashboard\SystemAuthentication;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator ;
use Illuminate\View\View;

class AdminLoginController extends Controller
{
    /**
     * Get Login View Form
     *
     * @return Factory|RedirectResponse|View
     */
    public function showAdminView()
    {
        if (! auth('web')->check()) {
            return view('Dashboard.SystemAuthentication.login');
        }

        return redirect()->intended('/admin');
    }

    /**
     * Login to Dashboard
     *
     * @param Request $request
     *
     * @return JsonResponse|RedirectResponse|Redirector
     */
    public function LoginToDashboard(Request $request)
    {
        if (auth('web')->check() == true) {
            return redirect(route('adminHomePage'));
        }

        // Validate Request.
        $request->validate([
            'email_address'    => 'required|email',
            'password'         => 'required',
        ]);

        // Trying to login with incoming data on guard ( web => admin ).
        if (auth('web')->attempt(['email_address' => $request->email_address , 'password' => $request->password])) {
            return redirect(route('adminHomePage'));
        }

        // Login Failed so we'll redirect back to login view with error message incorrect credentials.
        return back()->with([
            'danger' => 'Incorrect Email or password.'
        ]);
    }
}
