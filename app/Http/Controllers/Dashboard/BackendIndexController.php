<?php

namespace App\Http\Controllers\Dashboard;


class BackendIndexController
{
    /**
     * GET DASHBOARD HOME
     */
    public function index()
    {
        return view('Dashboard.index');
    }
}