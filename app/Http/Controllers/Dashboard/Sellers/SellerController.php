<?php


namespace App\Http\Controllers\Dashboard\Sellers;


use App\Models\Seller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SellerController
{
    /**
     * Get List of Available Sellers
     */
    public function index()
    {
        // Make a query to get all Sellers.
        $sellers = Seller::paginate(5);

        return view('Dashboard.Sellers.index', compact('sellers'));
    }


    /**
     * Get
     *
     * @return Factory|View
     */
    public function create()
    {
        return View('Dashboard.Sellers.create');
    }


    /**
     * @param Request $request
     *
     * @return JsonResponse|RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'           => 'required|max:255',
            'summary'        => 'required|max:500',
            'address'        => 'required|max:500',
            'email_address'  => 'required|email',
            'phone_number'   => 'required',
        ]);

        Seller::create($request->all());

        return redirect(route('sellers.index'))->with([
            'success' => 'Seller information Saved Successfully.'
        ]);
    }


    /**
     * Edit Seller Information ( Get View Form to edit ).
     *
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {


        // Make a Query to get seller by ID
        $seller = Seller::findOrFail($id);

        return view('Dashboard.Sellers.edit', compact('seller'));
    }


    /**
     * Update Seller information.
     *
     * @param Request $request
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'           => 'required|max:255',
            'summary'        => 'required|max:500',
            'address'        => 'required|max:500',
            'email_address'  => 'required|email',
            'phone_number'   => 'required',
        ]);

        // Find by id or fails ( get 404 ) error
        $seller = Seller::findOrFail($id);

        // update date
        $seller->update($request->all());

        return redirect(route('sellers.index'))->with([
            'success' => 'Seller information Updated Successfully.'
        ]);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|RedirectResponse
     */
    public function destroy($id)
    {
        $seller = Seller::findOrFail($id);
        $seller->delete();

        return redirect(route('sellers.index'))->with([
            'success' => 'Seller information Deleted Successfully.'
        ]);
    }
}