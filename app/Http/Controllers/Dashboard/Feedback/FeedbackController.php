<?php


namespace App\Http\Controllers\Dashboard\Feedback;


use App\Models\systemFeedback;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class FeedbackController
{
    /**
     * Get List of system feedback.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $feedback = systemFeedback::latest()->paginate();

        return view('Dashboard.Feedback.index', compact('feedback'));
    }

    /**
     * Delete a Feedback.
     *
     * @param $feedback_id
     *
     * @return RedirectResponse
     */
    public function destroy($feedback_id)
    {
        systemFeedback::findOrFail($feedback_id)->delete();

        return back()->with(['success' => 'Feedback has been deleted successfully!']);
    }
}
