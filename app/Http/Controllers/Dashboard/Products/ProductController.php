<?php


namespace App\Http\Controllers\Dashboard\Products;


use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Seller;
use App\Models\SubCategories;
use App\Traits\FileUploader;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProductController
{
    use FileUploader;

    /**
     * Get List of Available Products
     */
    public function index()
    {
        // Make a query to get all Products.
        $products = Product::paginate(5);

        return view('Dashboard.Products.index', compact('products'));
    }


    /**
     * Get Create form view.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Category::get();
        $sellers = Seller::pluck('name', 'id');
        $categories = $this->formatCategoriesToOptGroupSelect($categories);

        return View('Dashboard.Products.create', compact('categories', 'sellers'));
    }


    /**
     * Get User Details.
     *
     * @param $product_id
     *
     * @return Factory|View
     */
    public function show($product_id)
    {
        $product = Product::findOrFail($product_id);
        $productAvgReviews = $product->reviews()->avg('rating');

        return view('Dashboard.Products.show', compact('product', 'productAvgReviews'));
    }


    /**
     * @param Request $request
     *
     * @return JsonResponse|RedirectResponse
     */
    public function store(Request $request)
    {
        // Validate Request
        $request->validate($this->getValidationsRules());

        // Merge Parent Category id with our request to save it with product details.
        $request->merge([
            'category_id' => SubCategories::where('id', $request->sub_category_id)->first()->category_id
        ]);

        // Create and Save Product in a Database.
        $product = Product::create($request->all());

        // Upload images And assign them to the product in a Database.
        $this->saveAndUploadProductImages($product, $request);

        $product->productSeller()->sync([
            $product->id => [
                'seller_id'  => $request->seller_id,
                'sell_price' => $request->sell_price
            ]
        ]);

        return redirect(route('products.index'))->with(['success' => 'Product Saved Successfully.']);
    }


    /**
     * Edit Product Information ( Get View Form to edit ).
     *
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {
        // Make a Query to get Product by ID
        $product = Product::with('productSeller')->findOrFail($id);

        $categories = Category::get();
        $categories = $this->formatCategoriesToOptGroupSelect($categories);

        $sellers = Seller::pluck('name', 'id');

        return view('Dashboard.Products.edit', compact('product', 'categories', 'sellers'));
    }


    /**
     * Update Product information.
     *
     * @param Request $request
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        // Validate Request
        $request->validate($this->getValidationsRules('edit'));

        // Find by id or fails ( get 404 ) error
        $product = Product::findOrFail($id);

        // Merge Parent Category id with our request to save it with product details.
        $request->merge([
            'category_id' => SubCategories::where('id', $request->sub_category_id)->first()->category_id
        ]);

        // Update date
        $product->update($request->all());

        $product->productSeller()->sync([
            $product->id => [
                'seller_id'  => $request->seller_id,
                'sell_price' => $request->sell_price
            ]
        ]);

        // Upload images And assign them to the product in a Database.
        $this->saveAndUploadProductImages($product, $request);

        return redirect(route('products.index'))->with(['success' => 'Product Updated Successfully.']);
    }



    /**
     * @param $id
     *
     * @return JsonResponse|RedirectResponse
     */
    public function destroy($id)
    {
        // Make a Query to get Product by ID
        $product = Product::findOrFail($id);

        // Delete Images from Public Folder ( Uploads folder )
        foreach ($product->images as $image) {
           if (file_exists($file = public_path('/Uploads/Products/'.$image->image_path))) {
               unlink($file);
           }
        }
        // Delete Images from Public Folder ( Uploads folder )

        // Delete Product
        $product->delete();

        return redirect(route('products.index'))->with(['success' => 'Product Deleted Successfully.']);
    }

    /**
     * @param $categories
     *
     * @return array
     */
    public function formatCategoriesToOptGroupSelect($categories)
    {
        $results = [];

        /*
         *  We need format data to be as below ( to view it as opt group select ).
         *   "Mobiles" => [
                1 => "Apple"
                2 => "Samsung"
                3 => "Huawei"
              ],
              "Cameras" => [
                2 => "Cameras"
                4 => "Canon"
                5 => "Nikon"
                6 => "Panasonic"
              ]
         *
         * */

        foreach ($categories as $category) {

            $results[$category['name']][$category->id] = $category['name'];

            foreach ($category->subCategories as $subCategory) {
                $results[$category['name']][$subCategory->id] = $subCategory['name'];
            }
        }

        return $results;
    }


    /**
     * @param $product
     *
     * @param Request $request
     *
     * @return bool
     */
    public function saveAndUploadProductImages($product, Request $request)
    {
        // Start section Upload Images

        $imagesPaths = [];

        if ($request->hasFile('images')) {
            foreach($request->file('images') as $file) {
                $imagesPaths[] = $this->uploadFile($file, 'Products');
            }
        }

        // End section Upload Images

        // If we have images Save them to our Product.
        if ($imagesPaths) {
            // Assign the Uploaded image to this Product
            foreach($imagesPaths as $file) {
                $product->images()->create([
                    'image_path' => $file
                ]);
            }
        }

        return true;
    }


    /**
     * Delete Product Image by name and id
     *
     * @param $image_id
     *
     * @param $image_name
     *
     * @return RedirectResponse
     */
    public function deleteProductImage($image_id, $image_name)
    {
        $isDeleted = ProductImage::where(['id' => $image_id, 'image_path' => $image_name,])->delete();

        if ($isDeleted) {
            return back()->with(['info' => 'Image was deleted successfully.']);
        }

        return back();
    }


    /**
     * Get Product Validations rules.
     *
     * @param string $type
     * @return array
     */
    public function getValidationsRules($type = 'add'): array
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'seller_id' => 'required|exists:sellers,id',
            'images' => $type == ('add') ? 'required|array|min:1' : '',
            'images.*' => $type == ('add') ? 'required|image|max:1024' : '' // must be Valid image and max size is 1 MB.
        ];
    }
}
