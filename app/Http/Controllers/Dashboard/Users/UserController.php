<?php


namespace App\Http\Controllers\Dashboard\Users;


use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class UserController
{
    /**
     * Get System Users List
     */
    public function index()
    {
        $users = User::get();

        return view('Dashboard.Users.index', compact('users'));
    }

    /**
     * Get User Details.
     *
     * @param $user_id
     *
     * @return Factory|View
     */
    public function show($user_id)
    {
        $user = User::findOrFail($user_id);

        return view('Dashboard.Users.show', compact('user'));
    }
}