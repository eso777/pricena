<?php


namespace App\Http\Controllers\Dashboard\Categories;


use App\Models\Category;
use App\Models\SubCategories;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class CategoryController
{

    /**
     * Get List of Available Categories
     */
    public function index()
    {
        // Make a query to get all categories.
        $categories = Category::get();

        // Return results to view to start render in the front-end
        return view('Dashboard.Categories.index', compact('categories'));
    }


    /**
     * Get list for all sub categories belongs to the given main Category id
     *
     * @param $category_id
     *
     * @return Factory|View
     */
    public function show($category_id)
    {
        // Make a query to get all sub categories that belong to given main category.
        $subCategories = SubCategories::where('category_id', $category_id)->get();

        // Return results to view to start render in the front-end
        return view('Dashboard.Categories.show', compact('subCategories'));
    }
}