<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;

class SystemAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
           'name'          => 'Admin',
           'email_address' => 'admin@gmail.com',
           'password'      => bcrypt(123456),
           'phone_number'  => '0123456789',
        ]);
    }
}
