<?php

use Illuminate\Database\Seeder;

class SubCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \App\Models\SubCategories::insert([
          [
            'name' => 'Apple',
            'category_id' => 1 // mobiles
          ],
           [
            'name' => 'Samsung',
            'category_id' => 1 // mobiles
           ],
           [
                'name' => 'Huawei',
                'category_id' => 1 // mobiles
           ],

           [
               'name' => 'Canon',
               'category_id' => 2 // Cameras
           ],
           [
               'name' => 'Nikon',
               'category_id' => 2 // Cameras
           ], [
               'name' => 'Panasonic',
               'category_id' => 2 // Cameras
           ],
       ]);
    }
}
